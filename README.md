# media_entity_download_count

CONTENTS OF THIS FILE
---------------------

 * INTRODUCTION
 * REQUIREMENTS
 * DEPENDENCIES
 * INSTALLATION
 * MAINTAINERS

## INTRODUCTION
This module helps to record the number of the count of media entity file.

## REQUIREMENTS
- Drupal 8, 9, 10 or 11

## DEPENDENCIES
Media Entity Download

## INSTALLATION
Through Composer
composer require drupal/media_entity_download_count

OR

Through Manually
1. Copy Media Entity Download Count to the modules directory of your Drupal 
   installation.
2. Enable the 'Media Entity Download Count' module in 'Extend'. (/admin/modules)


## MAINTAINERS
Original development: Vipin Mittal(https://www.drupal.org/u/vipinmittal18)
