<?php

namespace Drupal\media_entity_download_count\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Configure WalkMe settings for this site.
 *
 * @package Drupal\media_entity_download_count\Form
 */
class MediaEntityDownloadCountSettings extends ConfigFormBase {

  /**
   * Set WalkMe config settings.
   *
   * @var string
   */
  const CONFIG_SETTINGS = 'media_entity_download_count.settings';

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      static::CONFIG_SETTINGS,
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'config_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_SETTINGS);
    $form['excluded file extensions'] = [
      '#type' => 'details',
      '#title' => $this->t('Excluded file extensions'),
      '#open' => TRUE,
    ];
    $form['excluded file extensions']['download_count_excluded_file_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Excluded file extensions'),
      '#default_value' => $config->get('download_count_excluded_file_extensions'),
      '#maxlength' => 255,
      '#description' => $this->t("To exclude files of certain types, enter the extensions to exclude separated by spaces. This is useful if you have private image fields and don't wish to include them in download counts."),
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config(static::CONFIG_SETTINGS)
      ->set('download_count_excluded_file_extensions', $form_state->getValue('download_count_excluded_file_extensions'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
